package com.example.calculatorag1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), View.OnClickListener {

    private var firstNumber = 0.0
    private var secondNumber = 0.0
    private var operation = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }
    private fun init(){
        button0.setOnClickListener(this)
        button1.setOnClickListener(this)
        button2.setOnClickListener(this)
        button3.setOnClickListener(this)
        button4.setOnClickListener(this)
        button5.setOnClickListener(this)
        button6.setOnClickListener(this)
        button7.setOnClickListener(this)
        button8.setOnClickListener(this)
        button9.setOnClickListener(this)

        buttonDot.setOnClickListener {
            val value = displayNumberTextView.text.toString()
            if (value.isNotEmpty() && ("." !in value)){
                displayNumberTextView.text = displayNumberTextView.text.toString() + "."
            }
        }
        deleteButton.setOnLongClickListener {
            displayNumberTextView.text = ""
            true
        }
    }

    fun equal(view: View) {
        val value = displayNumberTextView.text.toString()
        if (value.isNotEmpty() && operation != "") {
            secondNumber = value.toDouble()
            var result = 0.0
            if(operation == ":") {
                if (secondNumber == 0.0){
                    Toast.makeText(this, "Error", Toast.LENGTH_SHORT).show()
                }else{
                    result = firstNumber / secondNumber
                }
            }else if (operation == "x"){
                result = firstNumber * secondNumber
            }else if (operation == "-"){
                result = firstNumber - secondNumber
            }else if (operation == "+"){
                result = firstNumber + secondNumber
            }

            if (result % 1.0 == 0.0){
                displayNumberTextView.text = result.toInt().toString()
            }else{
                displayNumberTextView.text = result.toString()
            }
        }
    }

    fun delete(view: View){
        val resultNumber = displayNumberTextView.text.toString()
        if(resultNumber.isNotEmpty())
            displayNumberTextView.text = resultNumber.substring(0, resultNumber.length -1)

    }
    fun division(view: View) {
        val value = displayNumberTextView.text.toString()
        if (value.isNotEmpty()) {
            operation = ":"
            firstNumber = value.toDouble()
            displayNumberTextView.text = ""
        }
    }

    fun multiplication(view: View){
        val value = displayNumberTextView.text.toString()
        if(value.isNotEmpty()){
            operation = "x"
            firstNumber = value.toDouble()
            displayNumberTextView.text = ""
        }
    }
    fun subtraction(view: View){
        val value = displayNumberTextView.text.toString()
        if(value.isNotEmpty()){
            operation = "-"
            firstNumber = value.toDouble()
            displayNumberTextView.text = ""
        }
    }
    fun addition(view: View){
        val value = displayNumberTextView.text.toString()
        if(value.isNotEmpty()){
            operation = "+"
            firstNumber = value.toDouble()
            displayNumberTextView.text = ""
        }
    }

    override fun onClick(v: View?) {
        val buttonNumber = v as Button
        if (displayNumberTextView.text.toString() == "0"){
            displayNumberTextView.text = "0"
        }else{
            displayNumberTextView.text = displayNumberTextView.text.toString() + buttonNumber.text.toString()
        }

    }
}